<?php
/**
 * Created by PhpStorm.
 * User: Deividas
 * Date: 2018-07-16
 * Time: 12:00
 */

namespace App\Controller;

use App\Entity\Idea;
use App\Entity\Image;
use App\Entity\Partner;
use App\Form\ChangePasswordType;
use App\Form\IdeaType;
use App\Form\ImageType;
use App\Form\PartnerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DashboardController extends BaseController
{

    /**
     * @Route("/dashboard/", name="dashboard")
     */
    public function viewIndex(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $setting = $em->getRepository('App:Setting');

        if ($this->handleSeoForm($request)) {

            $em->persist($setting->findOneBy(['name' => 'title'])->setValue($request->get('title')));
            $em->persist($setting->findOneBy(['name' => 'meta-keywords'])->setValue($request->get('keywords')));
            $em->persist($setting->findOneBy(['name' => 'meta-description'])->setValue($request->get('description')));

            $em->flush();
        }

        if ($this->handleContactsForm($request)) {

            $em->persist($setting->findOneBy(['name' => 'phone-number'])->setValue($request->get('phone-number')));
            $em->persist($setting->findOneBy(['name' => 'email'])->setValue($request->get('email')));

            $em->flush();
        }

        return $this->render(
            "dashboard/index.html.twig"
        );

    }

    private function handleSeoForm(Request $request) {

        if ($request->isMethod('POST')
            && isset($_POST['title'])
            && isset($_POST['meta-keywords'])
            && isset($_POST['meta-description'])
        ) return true;

        else return false;
    }

    private function handleContactsForm(Request $request) {

        if ($request->isMethod('POST')
            && isset($_POST['email'])
            && isset($_POST['phone-number'])
        ) return true;

        else return false;
    }

    /**
     * @Route("/dashboard/profile/", name="dashboard-profile")
     */
    public function profilePage(Request $request, UserPasswordEncoderInterface $encoder) {

        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('dashboard');
        }

        return $this->render(
            "dashboard/profile.html.twig",
            [
                'form' => $form->createView()
            ]
        );

    }

    /**
     * @Route("/dashboard/images/{id}/", name="ideas-images")
     */
    public function imagesPage(Request $request, int $id) {

        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('App:Image')->findBy(['idea' => $id]);

        $idea = $em->getRepository('App:Idea')->findOneBy(['id' => $id]);

        $image = new Image();
        $form = $this->createForm(ImageType::class, $image);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $image->setIdea($idea);
            $image->setPath($request->getSchemeAndHttpHost().'/assets/images/ideas/'.uniqid().'.'.$image->getFile()->guessExtension());
            $image->upload();

            $em->persist($image);
            $em->flush();

            return $this->redirectToRoute('ideas-images', ['id' => $id]);
        }

        return $this->render(
            "dashboard/ideas/images.html.twig",
            [
                'idea' => $idea,
                'images' => $images,
                'form' => $form->createView()
            ]
        );

    }

    /**
     * @Route("/dashboard/ideas/{idea_id}/images/{image_id}/remove/", name="ideas-images-remove")
     */
    public function removeImage(int $idea_id, int $image_id) {

        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository('App:Image')->findOneBy(['id' => $image_id]);

        if ($image == null)
            throw new NotFoundHttpException();

        $absolute_path = explode('/', $image->getPath());
        if (file_exists(__DIR__.'/../../public/assets/images/ideas/'.$absolute_path[6]))
        {
            unlink(__DIR__.'/../../public/assets/images/ideas/'.$absolute_path[6]);
        }

        $em->remove($image);
        $em->flush();

        return $this->redirectToRoute('ideas-images', ['id' => $idea_id]);
    }

    /**
     * @Route("/dashboard/ideas/", name="dashboard-ideas")
     */
    public function ideasPage(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $ideas = $em->getRepository('App:Idea')->findAll();

        $idea = new Idea();
        $form = $this->createForm(IdeaType::class, $idea);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($idea);
            $em->flush();

            return $this->redirectToRoute('dashboard-ideas');
        }

        return $this->render(
            "dashboard/ideas/ideas.html.twig",
            [
                'ideas' => $ideas,
                'form' => $form->createView()
            ]
        );

    }

    /**
     * @Route("/dashboard/partners/", name="dashboard-partners")
     */
    public function partnersPage(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $partners = $em->getRepository('App:Partner')->findAll();

        $partner = new Partner();
        $form = $this->createForm(PartnerType::class, $partner);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $partner->setPath($request->getSchemeAndHttpHost().'/assets/images/partners/'.uniqid().'.'.$partner->getFile()->guessExtension());
            $partner->upload();

            $em->persist($partner);
            $em->flush();

            return $this->redirectToRoute('dashboard-partners');
        }

        return $this->render(
            "dashboard/partners.html.twig",
            [
                'partners' => $partners,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/dashboard/partners/{id}/remove/", name="partner-remove")
     */
    public function removePartner(int $id) {

        $em = $this->getDoctrine()->getManager();
        $partner = $em->getRepository('App:Partner')->findOneBy(['id' => $id]);

        if ($partner == null)
            throw new NotFoundHttpException();

        $absolute_path = explode('/', $partner->getPath());
        if (file_exists(__DIR__.'/../../public/assets/images/partners/'.$absolute_path[6]))
        {
            unlink(__DIR__.'/../../public/assets/images/partners/'.$absolute_path[6]);
        }

        $em->remove($partner);
        $em->flush();

        return $this->redirectToRoute('dashboard-partners');
    }


    /**
     * @Route("/dashboard/ideas/{id}/edit/", name="dashboard-ideas-edit")
     */
    public function editIdea(Request $request, int $id) {

        $em = $this->getDoctrine()->getManager();
        $idea = $em->getRepository('App:Idea')->findOneBy(['id' => $id]);

        if ($idea == null)
            throw new NotFoundHttpException();

        $form = $this->createForm(IdeaType::class, $idea);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($idea);
            $em->flush();

            return $this->redirectToRoute('dashboard-ideas');
        }
        return $this->render(
            "dashboard/ideas/ideas_edit.html.twig",
            [
                'form' => $form->createView()
            ]
        );

    }

    /**
     * @Route("/dashboard/ideas/{id}/remove/", name="dashboard-ideas-remove")
     */
    public function removeIdea(int $id) {

        $em = $this->getDoctrine()->getManager();
        $idea = $em->getRepository('App:Idea')->findOneBy(['id' => $id]);

        if ($idea == null)
            throw new NotFoundHttpException();

        $em->remove($idea);
        $em->flush();

        return $this->redirectToRoute('dashboard-ideas');
    }
}