<?php
/**
 * Created by PhpStorm.
 * User: Deividas
 * Date: 2018-06-27
 * Time: 00:04
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\FileValidator;
use Symfony\Component\Validator\Constraints\ImageValidator;

class MainController extends BaseController
{

    /**
     * @Route("/", name="homepage")
     */
    public function viewHomePage() {

        $partners = $this->getDoctrine()->getRepository('App:Partner')->findAll();

        return $this->render(
            "content.html.twig",
            ['partners' => $partners]
        );

    }

    /**
     * @Route("/kontaktai/", name="contacts")
     */
    public function viewContactsPage(Request $request) {

        $form = $this->createFormBuilder()
            ->add('name', TextType::class,
                [
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Jūsų vardas'
                    ]
                ]
            )
            ->add('email', EmailType::class,
                [
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'El. paštas'
                    ]
                ]
            )
            ->add('message', TextareaType::class,
                [
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Žinutė',
                        'style' => 'height:150px'
                    ]
                ]
            )
            ->add('file', FileType::class,
                [
                    'multiple' => true,
                    'label' => false,
                    'required' => false
                ]
            )
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $admins = $em->getRepository('App:Account')->findByRole('ROLE_ADMIN');

            $emails = array();
            foreach ($admins as $admin)
            {
                $emails[] = $admin->getEmail();
            }

            $transport = (new \Swift_SmtpTransport('smtp.gmail.com', 587, 'tls'))
                ->setUsername(getenv('MAILER_USERNAME'))
                ->setPassword(getenv('MAILER_PASSWORD'));

            $mailer = new \Swift_Mailer($transport);
            $message = (new \Swift_Message('Dailidės darbai - žinutė nuo '.$data['name']))
                ->setFrom([$data['email']])
                ->setTo($emails)
                ->setBody($data['message'])
            ;

            foreach ($form['file']->getData() as $file){
                $message->attach(\Swift_Attachment::fromPath($file)->setFilename($file->getClientOriginalName()));
            }
            if (!$mailer->send($message))
                throw new NotFoundHttpException('Something went wrong with Mailer!');
        }

        return $this->render(
            "pages/contacts.html.twig",
            [
                'form' => $form->createView()
            ]
        );

    }

    /**
     * @Route("/idejos/", name="ideas")
     */
    public function viewIdeasPage() {

        $ideas = $this->getDoctrine()->getManager()->getRepository('App:Idea')->findAll();

        return $this->render(
            "pages/ideas.html.twig",
            [
                'ideas' => $ideas
            ]
        );

    }
}