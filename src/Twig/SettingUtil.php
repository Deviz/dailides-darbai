<?php
namespace App\Twig;

use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SettingUtil extends AbstractExtension
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getFunctions(): array {
        return array(
            new TwigFunction('getSetting', array($this, 'getSetting'))
        );
    }

    public function getSetting($name)
    {
        $setting = $this->em->getRepository('App:Setting')->findOneBy(['name' => $name]);

        if ($setting == null)
            return '';

        switch ($setting->getType())
        {
            case 'boolean':
                if ($setting->getValue() === 'true')
                    return true;
                else
                    return false;

            case 'integer':
                return (int)$setting->getValue();

            case 'string':
                return (string)$setting->getValue();

            default:
                return '';
        }
    }
}