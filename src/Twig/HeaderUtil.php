<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Symfony\Component\HttpFoundation\RequestStack;

class HeaderUtil extends AbstractExtension
{

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function getFilters()
    {
        return array(
            new TwigFilter("routeActive", array($this, 'routeFilter')),
        );
    }

    public function routeFilter($routeName)
    {
        $request = $this->requestStack->getCurrentRequest();

        if ($request->get("_route") == $routeName)
        {
            if (strpos($routeName, 'dashboard') !== false)
                return "active";
            else
                return "menu-active";
        }

        return "";
    }

}