<?php declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180718094747 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE images DROP INDEX UNIQ_E01FBE6A5B6FEF7D, ADD INDEX IDX_E01FBE6A5B6FEF7D (idea_id)');
        $this->addSql('ALTER TABLE images DROP type');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE images DROP INDEX IDX_E01FBE6A5B6FEF7D, ADD UNIQUE INDEX UNIQ_E01FBE6A5B6FEF7D (idea_id)');
        $this->addSql('ALTER TABLE images ADD type INT NOT NULL');
    }
}
