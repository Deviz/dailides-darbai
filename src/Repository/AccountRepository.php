<?php
/**
 * Created by PhpStorm.
 * User: Deividas
 * Date: 2018-07-16
 * Time: 15:22
 */

namespace App\Repository;

use App\Entity\Account;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AccountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Account::class);
    }

    public function findByRole(string $role): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u')
            ->from("App:Account", 'u')
            ->where($qb->expr()->like('u.roles', $qb->expr()->literal("%$role%")))
        ;
        return $qb->getQuery()->getResult();
    }
}