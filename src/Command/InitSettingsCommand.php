<?php
/**
 * Created by PhpStorm.
 * User: Deividas
 * Date: 2018-02-14
 * Time: 19:24
 */

namespace App\Command;

use App\Entity\Setting;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class InitSettingsCommand extends ContainerAwareCommand
{
    private $em;
    private $passwordEncoder;

    public function __construct(?string $name = null, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($name);

        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure()
    {
        $this
            ->setName('app:init-settings')
            ->setDescription('Sets default settings.');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $title = new Setting('title', 'Dailidės darbai - Pradžia');
        $keywords = new Setting('meta-keywords', 'dailidesdarbai, dailides darbai, mediniai stogeliai, lauko baldai, vidaus baldai, medzio dirbiniai');
        $description = new Setting('meta-description', 'Gaminame nestandartinius medienos gaminius pagal jūsų pageidavimą. Tai gaminiai suteikiantys jūsų namams jaukumo!');

        $email = new Setting('email', 'info@dailidesdarbai.lt');
        $phonenb = new Setting('phone-number', '+370 60363480');

        $this->em->persist($title);
        $this->em->persist($keywords);
        $this->em->persist($description);
        $this->em->persist($email);
        $this->em->persist($phonenb);
        $this->em->flush();

        $output->writeln('Settings was successfully created and default information saved!');
    }


}
