<?php
/**
 * Created by PhpStorm.
 * User: Deividas
 * Date: 2018-02-14
 * Time: 19:24
 */

namespace App\Command;

use App\Entity\Account;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Zend\Code\Exception\RuntimeException;

class GenerateUserCommand extends ContainerAwareCommand
{
    private $em;
    private $passwordEncoder;

    public function __construct(?string $name = null, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($name);

        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure()
    {
        $this
            ->setName('app:generate-user')
            ->addArgument('email', InputArgument::REQUIRED)
            ->setDescription('Creates account.');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument("email");

        if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            throw new RuntimeException('Not valid email!');

        $account = new Account();

        $plainPassword = bin2hex(random_bytes(16));
        $password = $this->passwordEncoder->encodePassword($account, $plainPassword);

        $account
            ->setEmail($email)
            ->setPassword($password)
            ->setRoles(['ROLE_ADMIN']);

        $this->em->persist($account);
        $this->em->flush();

        $output->writeln('User was successfully created and information email sent!');
        $output->writeln('Generated password: '.$plainPassword);
    }

    protected function sendEmail(string $recipient)
    {
        $transport = (new \Swift_SmtpTransport('smtp.gmail.com', 587, 'tls'))
            ->setUsername(getenv('MAILER_USERNAME'))
            ->setPassword(getenv('MAILER_PASSWORD'));

        $mailer = new \Swift_Mailer($transport);
        $message = (new \Swift_Message('Dailidės darbai - Paskyros informacija'))
            ->setFrom(['info@dailidesdarbai.lt' => 'Dailidės darbai'])
            ->setTo($recipient)
            ->setBody($this->getContainer()->get('templating')->renderView('emails/generated_account.html.twig'), 'text/html')
        ;
        if (!$mailer->send($message))
            throw new RuntimeException('Something went wrong with Mailer!');

    }
}
