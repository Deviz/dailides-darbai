<?php
/**
 * Created by PhpStorm.
 * User: Deividas
 * Date: 2018-07-17
 * Time: 15:06
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="partners")
 * @ORM\Entity(repositoryClass="App\Repository\PartnerRepository")
 */
class Partner
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $link;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $path;

    /**
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/png", "image/jpeg", "image/jpg"}
     * )
     */
    public $file;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    public function getPath() : string
    {
        return $this->path;
    }

    public function setPath($path) : Partner
    {
        $this->path = $path;

        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file): Partner
    {
        $this->file = $file;

        return $this;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLink($link): Partner
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        $absolute_path = explode('/', $this->getPath());

        $this->file->move($this->getUploadDir(), $absolute_path[6]);
    }

    private function getUploadDir() {
        return __DIR__.'/../../public/assets/images/partners/';
    }
}
