<?php
/**
 * Created by PhpStorm.
 * User: Deividas
 * Date: 2018-07-17
 * Time: 15:06
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="images")
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 */
class Image
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $path;

    /**
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/png", "image/jpeg", "image/jpg"}
     * )
     */
    public $file;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Idea", inversedBy="images")
     */
    private $idea;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPath() : string
    {
        return $this->path;
    }

    public function setPath($path) : Image
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file): Image
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdea()
    {
        return $this->idea;
    }

    public function setIdea($idea): Image
    {
        $this->idea = $idea;

        return $this;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        $absolute_path = explode('/', $this->getPath());

        $this->file->move($this->getUploadDir(), $absolute_path[6]);
    }

    private function getUploadDir() {
        return __DIR__.'/../../public/assets/images/ideas/';
    }
}
