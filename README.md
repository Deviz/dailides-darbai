# Dailidės darbai

Projekto parengimo naudojimui intrukcijos:

> git clone https://gitlab.com/Deviz/dailides-darbai

> composer install

> npm run winpack

**Redaguoti .env failą ir pakeisti mysql ir mailer prisijungimo duomenis**

> php bin/console doctrine:migrations:migrate

> php bin/console app:init-settings

> php bin/console app:generate-user

Užsižymėti sugeneruotą slaptažodį ir prisijungus prie **https://adresas.lt/login** pakeisti slaptažodį.