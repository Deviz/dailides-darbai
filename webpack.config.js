const path = require("path");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {

    plugins: [
        new UglifyJsPlugin()
    ],

    entry: {
        frontend: path.join(__dirname, "./assets/js/index.js"),
        login: path.join(__dirname, "assets/js/login.js"),
        dashboard: path.join(__dirname, "assets/js/dashboard.js")
    },

    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "public"),
        publicPath: "/"
    },

    module: {

        loaders: [

            {
                test: /\.js?/,
                loader: 'babel-loader'
            },

            {
                test: /\.(eot|svg|ttf|woff|woff2|html)$/,
                loader: 'file-loader',
                options: {
                    name: 'assets/fonts/[name].[ext]'
                }
            },

            {
                test: /\.(scss|css)$/,
                use: [{
                    loader: "style-loader" // creates style nodes from JS strings
                }, {
                    loader: "css-loader" // translates CSS into CommonJS
                }, {
                    loader: "sass-loader" // compiles Sass to CSS
                }]
            },

            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                loader: 'file-loader',
                options: {
                    name: 'assets/images/[name].[ext]'
                }
            }


        ]

    }
};